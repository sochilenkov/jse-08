package ru.t1.sochilenkov.tm.repository;

import ru.t1.sochilenkov.tm.api.ICommandRepository;
import ru.t1.sochilenkov.tm.constant.ArgumentConstant;
import ru.t1.sochilenkov.tm.constant.CommandConstant;
import ru.t1.sochilenkov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(CommandConstant.HELP, ArgumentConstant.HELP, "Show help info");

    public static final Command VERSION = new Command(CommandConstant.VERSION, ArgumentConstant.VERSION, "Show version info");

    public static final Command ABOUT = new Command(CommandConstant.ABOUT, ArgumentConstant.ABOUT, "Show developer info");

    public static final Command INFO = new Command(CommandConstant.INFO, ArgumentConstant.INFO, "Show system info");

    public static final Command EXIT = new Command(CommandConstant.EXIT, null, "Close application");

    public static final Command[] COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, INFO, EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
