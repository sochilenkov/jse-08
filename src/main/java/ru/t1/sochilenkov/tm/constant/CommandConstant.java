package ru.t1.sochilenkov.tm.constant;

public final class CommandConstant {

    public static final String HELP = "help";

    public static final String VERSION = "version";

    public static final String ABOUT = "about";

    public static final String INFO = "info";

    public static final String EXIT = "exit";

    private CommandConstant() {
    }

}
